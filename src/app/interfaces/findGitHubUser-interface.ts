
export interface FindGitHubUserInterface {
    username: string,
    login: string,
    avatar_url: any,
    name: string,
    blog: string,
    location: string,
    twitter_username: string,
    followers: number,
    following: number,
    public_repos: number,
    created_at: string,
    message?: string
}