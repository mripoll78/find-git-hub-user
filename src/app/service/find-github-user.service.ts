import { environment } from 'src/environments/environment';
import { FindGitHubUserInterface } from '../interfaces/findGitHubUser-interface';

import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from "@angular/common/http";

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FindGithubUserService {
  env = environment

  constructor(private http: HttpClient) { }

  getUserGitHub(username: string) {
    return this.http.get<any>(this.env.urlGitHub + username).pipe(catchError(this.erroHandler));
  }

  erroHandler(error: HttpErrorResponse) {
    console.log("f");
    return throwError(() => new Error('test'));
  }
}
