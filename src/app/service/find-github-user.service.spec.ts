import { TestBed } from '@angular/core/testing';

import { FindGithubUserService } from './find-github-user.service';

describe('FindGithubUserService', () => {
  let service: FindGithubUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FindGithubUserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
