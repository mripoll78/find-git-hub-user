import { Component } from '@angular/core';
import { FindGitHubUserInterface } from './interfaces/findGitHubUser-interface';
import { FindGithubUserService } from './service/find-github-user.service';
import { DomSanitizer } from '@angular/platform-browser';
import { catchError, Observable, Subject, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'find-git-hub-user';
  env = environment

  constructor(private findGithubUserService: FindGithubUserService, private sanitizer: DomSanitizer, private http: HttpClient) { }

  isData: boolean = false
  loginEmpty: boolean = false
  search: boolean = false
  loadin: boolean = false

  public findGitHubUser: FindGitHubUserInterface = {
    username: "",
    login: "",
    avatar_url: "",
    name: "",
    blog: "",
    location: "",
    twitter_username: "",
    followers: 0,
    following: 0,
    public_repos: 0,
    created_at: "",
  }

  getUserGitHub() {
    this.loadin = true
    if (this.validateForm()) {
      this.get(this.findGitHubUser.username)
        .subscribe(
          data => {
            if (data) {
              this.isData = true;
              this.loadin = false;
              this.findGitHubUser.blog = data?.blog
              this.findGitHubUser.avatar_url = this.sanitizer.bypassSecurityTrustResourceUrl(data?.avatar_url)
              this.findGitHubUser.name = data?.name
              this.findGitHubUser.location = data?.location
              this.findGitHubUser.twitter_username = data?.twitter_username
              this.findGitHubUser.followers = data?.followers
              this.findGitHubUser.following = data?.following
              this.findGitHubUser.public_repos = data?.public_repos
              this.findGitHubUser.created_at = data?.created_at
              this.findGitHubUser.login = data?.login
            } else {
              this.isData = false;
              this.loadin = false;
            }
          }
        )
    }
  }

  validateForm() {
    if (this.findGitHubUser.username == "") {
      this.loginEmpty = true
      return false;
    } else {
      this.loginEmpty = false
    }

    return true
  }

  get(username: string) {
    return this.http.get<any>(this.env.urlGitHub + username).pipe(catchError((error: HttpErrorResponse) => {
      this.isData = false;
      this.loadin = false;
      console.log(error.message);
      return throwError(() => new Error('test'));
    }));
  }
}